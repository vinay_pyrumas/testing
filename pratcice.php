<?php
    $array1 = array(1,2);
    // $x = &$array1[1];   // Unused reference
    $array2 = $array1;  // reference now also applies to $array2 !
    $array2[1]=22;      // (changing [0] will not affect $array1)
    print_r($array2);
    echo"</br>";
	
    $array13 = array(1,2);
    $x = &$array13[1];
    $array2 = $array13;
    unset($x); // Array copy is now unaffected by above reference
    $array2[1]=22;
    print_r($array13);
    echo"</br>";
	
    $arr = array('a'=>'first', 'b'=>'second', 'c'=>'third');
    foreach ($arr as &$a);
    unset($a);// do nothing. maybe?
    foreach ($arr as $a);  // do nothing. maybe?
    print_r($arr);
	
	echo "INSERT INTO users (username) VALUES ";
    for($i=1;$i<= 160 ;$i++){
        echo "('$i')".",";
    }
    
	   function sum($a, $b): int {
        return $a + $b;
    } 
    
    var_dump(sum(1, 2));
    var_dump(sum(1, 2));
    echo"</br>";echo"</br>";
    }
    
	
	
	function add2ints(int $x, int $y):float
{
    $z = $x + $y;
    if ($z===0)
    {
        return 1;
    }
    return $z;
	}
	$a = NULL;
	echo is_null($a) ? 'Null' : $a;
	$b = add2ints(-2, 2);
	echo is_null($b) ? 'Null' : $b;
    echo"</br>";echo"</br>";
	
	
	
	echo '</br>';
    $month = 2;
    $year = 1994;
    $days = ($month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31)); //returns days in the given month
    echo $days;
    echo '</br>';
    $score = 10;
    $age = 20;
    echo 'Taking into account your age and score, you are: ',($age > 10 ? ($score < 80 ? 'behind' : 'above average') : ($score < 50 ? 'behind' : 'above average')); // returns 'You are behind'
    echo '</br>';
	   // fetch the value of $_GET['user'] and returns 'not passed'
   // if username is not passed
   $username = $_GET['username'] ?? 'not passed';
   print($username);
   print("<br/>");

   // Equivalent code using ternary operator
   $username = isset($_GET['username']) ? $_GET['username'] : 'not passed';
   print($username);
   print("<br/>");
   // Chaining ?? operation
   $username =  'username' ?? "vinay" ?? 'not passed';
   print($username);
   
   echo "\u{a12a}"."</br>";
   echo "\u{000ra}"."</br>";
   echo "\u{9999}"."</br>";

    class A {private $x = 1;}
	// Pre PHP 7 code
	$getX = function() {return $this->x;};
	$getXCB = $getX->bindTo(new A, 'A'); // intermediate closure
	echo $getXCB();

	// PHP 7+ code
	$getX = function() {return $this->x;};
	echo $getX->call(new A);
	
?>